"""Test kcidb serializers."""
import json

from django.core.serializers.json import DjangoJSONEncoder
from rest_framework.exceptions import ValidationError

from datawarehouse import models
from datawarehouse import serializers
from tests import utils


class TestIssueSerializer(utils.TestCase):
    # pylint: disable=too-many-instance-attributes
    """Test Issue Serializer serializers."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issue_with_kcidb.yaml',
    ]

    def setUp(self):
        """Setup for Issue Serializer tests."""
        self.test_json = json.load(open("tests/serializers/test_data/issue_serializer.json", "r"))

    def test_serialize_fixtures(self):
        """Check serialized json output with valid JSON data."""
        issues = models.Issue.objects.all().order_by('id')
        serializer = serializers.IssueSerializer(issues, many=True)
        json_serialized = json.dumps(serializer.data, cls=DjangoJSONEncoder)
        self.assertJSONEqual(json_serialized, self.test_json['valid_in_fixtures'])

    def test_invalid_data(self):
        """Check if validity check catches invalid data."""
        for invalid_check, test_data in self.test_json['invalid_check'].items():
            with self.subTest(invalid_check):
                self.assertFalse(serializers.IssueSerializer(data=test_data).is_valid())

    def test_first_seen(self):
        """Check Issue's first_seen property."""
        checkout = models.KCIDBCheckout.objects.last()
        issues = models.Issue.objects.all()

        for issue in issues:
            serialized = serializers.IssueSerializer(issue).data

            self.assertEqual(
                serialized['first_seen'],
                checkout.start_time
            )

    def test_first_seen_no_revision(self):
        """Check Issue's first_seen property when there's no checkout."""
        models.KCIDBCheckout.objects.all().delete()
        issues = models.Issue.objects.all()

        for issue in issues:
            serialized = serializers.IssueSerializer(issue).data

            self.assertIsNone(
                serialized['first_seen'],
            )

    def test_first_seen_no_start_time(self):
        """Check Issue's first_seen property when there's no start_time."""
        models.KCIDBCheckout.objects.update(start_time=None)
        issues = models.Issue.objects.all()

        for issue in issues:
            serialized = serializers.IssueSerializer(issue).data

            self.assertIsNone(
                serialized['first_seen'],
            )

    def test_last_seen(self):
        """Check Issue's last_seen property."""
        checkout = models.KCIDBCheckout.objects.first()
        issues = models.Issue.objects.all()

        for issue in issues:
            serialized = serializers.IssueSerializer(issue).data

            self.assertEqual(
                serialized['last_seen'],
                checkout.start_time
            )

    def test_last_seen_no_revision(self):
        """Check Issue's last_seen property when there's no checkout."""
        models.KCIDBCheckout.objects.all().delete()
        issues = models.Issue.objects.all()

        for issue in issues:
            serialized = serializers.IssueSerializer(issue).data

            self.assertIsNone(
                serialized['last_seen'],
            )

    def test_last_seen_no_start_time(self):
        """Check Issue's last_seen property when there's no start_time."""
        models.KCIDBCheckout.objects.update(start_time=None)
        issues = models.Issue.objects.all()

        for issue in issues:
            serialized = serializers.IssueSerializer(issue).data

            self.assertIsNone(
                serialized['last_seen'],
            )

    def test_create(self):
        """Check create method."""
        data = {
            "kind": 1,
            "description": "Issue Serializer create",
            "ticket_url": "http://example.com/issue_create",
            "resolved_at": None,
            "policy": 1
        }

        serializer = serializers.IssueSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        issue = models.Issue.objects.get(description="Issue Serializer create")

        self.assertEqual(issue.description, "Issue Serializer create")
        self.assertEqual(issue.kind, models.IssueKind.objects.get(tag="Issue"))
        self.assertEqual(issue.policy, models.Policy.objects.get(name="public"))


class TestPolicySerializer(utils.TestCase):
    # pylint: disable=too-many-instance-attributes
    """Test Policy Serializer serializers."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issue_with_kcidb.yaml',
    ]

    def test_save(self):
        """Check save method."""
        data = {
            "name": "public_new",
            "read_group": None
        }

        serializer = serializers.PolicySerializer(data=data)
        self.assertTrue(serializer.is_valid())
        created_obj = serializer.save()
        policy = models.Policy.objects.get(name="public_new")
        self.assertEqual(created_obj, policy)

        data = {
            "name": "g1_g2",
            "read_group": "1",
            "write_group": 2
        }

        serializer = serializers.PolicySerializer(data=data)
        self.assertTrue(serializer.is_valid())
        created_obj = serializer.save()
        policy = models.Policy.objects.get(name="g1_g2")
        self.assertEqual(created_obj, policy)

    def test_name_validation(self):
        """Check name validation."""
        data = {
            "name": "public"
        }

        serialized = serializers.PolicySerializer(data=data)
        self.assertFalse(serialized.is_valid())

        data = {
            "name": "public_new"
        }

        serialized = serializers.PolicySerializer(data=data)
        self.assertTrue(serialized.is_valid())

    def test_group_validation(self):
        """Check group validation."""
        data = {
            "name": "foobar",
            "read_group": 100
        }
        serialized = serializers.PolicySerializer(data=data)
        self.assertRaises(ValidationError, serialized.is_valid, raise_exception=True)

        data = {
            "name": "foobar",
            "write_group": "a"
        }
        serialized = serializers.PolicySerializer(data=data)
        self.assertRaises(ValidationError, serialized.is_valid, raise_exception=True)
