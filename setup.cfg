[metadata]
name = datawarehouse
description = Data warehouse for CKI pipelines
author = CKI Project
license = GPLv2+
version = 1

[options]
# Automatically find all files beneath the kpet directory and include them.
packages = datawarehouse
# Parse the MANIFEST.in file and include those files, too.
include_package_data = True
scripts =
    manage.py
    entrypoint.sh
install_requires =
    celery
    cki-lib @ git+https://gitlab.com/cki-project/cki-lib.git@production
    Django<4.2
    django-cors-headers # For frontend devel only
    django-debug-toolbar
    django-extensions
    django-prometheus
    django-simple-captcha
    djangorestframework
    djangosaml2
    ipython
    kcidb-io @ git+https://github.com/kernelci/kcidb-io.git@v3
    natsort
    ldap3
    pika # rabbitmq
    psycopg2-binary
    requests-futures # For patches downloading
    sentry-sdk
    uwsgi
    pymemcache

[options.extras_require]
test =
    responses
    flake8-django
    pylint-django
    pytest-django
    freezegun
    pylint
dev =
    pympler # For memory profiling
    tox<4
    datawarehouse[test]

[testenv]
passenv =
    DB_HOST
    DB_PASSWORD
    SECRET_KEY
    GITLAB_URL
    BEAKER_URL
    CKI_DEPLOYMENT_ENVIRONMENT
setenv =
    FF_SIGNUP_ENABLED=true
    CAPTCHA_TEST_MODE=true
    # Tests can't run with DEBUG=True, make sure settings comply
    # https://docs.djangoproject.com/en/4.1/topics/testing/overview/#other-test-conditions
    DEBUG=False
extras = test
commands = python3 manage.py check
           python3 manage.py makemigrations --dry-run --check
           cki_lint.sh datawarehouse

[tool:pytest]
DJANGO_SETTINGS_MODULE = datawarehouse.settings
addopts = --ignore=datawarehouse/models

[pylint.MAIN]
disable=duplicate-code,cyclic-import
load-plugins=pylint_django
django-settings-module = datawarehouse.settings
ignore=migrations
# After the upgrade to Fedora 37 in the gitlab runners, pylint has
# started reporting `no method` errors in function calls of these
# modules. Let's disable those checks in these modules until the problem
# is solved.
generated-members=os.*
ignored-modules=collections.abc*

[flake8]
# Django recommended
max-line-length = 119
per-file-ignores =
    datawarehouse/models/*.py:DJ01,DJ09
    datawarehouse/models/__init__.py:F401
    datawarehouse/scripts/__init__.py:F401
    **/urls.py:DJ05

[FORMAT]
max-line-length=119

[tox:tox]
