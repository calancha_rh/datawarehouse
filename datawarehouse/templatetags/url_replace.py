"""URL Replace."""
from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def url_replace(context, **kwargs):
    """Replace GET parameter on the current request."""
    query = context['request'].GET.copy()
    # If we use query.update instead of assigning the value
    # it appends them.
    for key, value in kwargs.items():
        query[key] = value
    return query.urlencode()


@register.simple_tag(takes_context=True)
def url_remove_param(context, *args):
    """Remove GET parameter from the current request."""
    query = context['request'].GET.copy()
    for key in args:
        try:
            del query[key]
        except KeyError:
            pass

    return query.urlencode()
