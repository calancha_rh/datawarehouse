"""Signals."""
from django import dispatch

# pylint: disable=invalid-name
# Signal arguments: status, object_type, objects.
kcidb_object = dispatch.Signal()
