# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods,too-many-public-methods

"""Pipeline models file."""
from django.db import models
from django_prometheus.models import ExportModelOperationsMixin as EMOM

from datawarehouse.models import utils


class GitTree(EMOM('git_tree'), utils.Model):
    """Model for GitTree."""

    name = models.CharField(max_length=50)

    path_to_policy = 'kcidbcheckout__policy'

    objects = utils.GenericNameManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'

    @classmethod
    def create_from_string(cls, name):
        """Create GitTree from string."""
        return cls.objects.get_or_create(name=name)[0]


class ArchitectureEnum(models.IntegerChoices):
    # pylint: disable=invalid-name
    """Enumeration of the possible architectures."""

    aarch64 = 2, 'aarch64'
    ppc64 = 3, 'ppc64'
    ppc64le = 4, 'ppc64le'
    s390x = 5, 's390x'
    x86_64 = 6, 'x86_64'
    i686 = 8, 'i686'
    noarch = 9, 'noarch'


class Compiler(EMOM('compiler'), models.Model):
    """Model for Compiler."""

    name = models.TextField()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'
